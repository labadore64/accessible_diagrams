
1. Introduction

This project is a small accessibility demo to demonstrate a diagram reader for blind and visually impaired people.

Move the cursor around - as you go over the diagram image, you will hear a sound that correlates with your position. This sound is both panned for X axis, and pitched for Y axis, so you can tell your position based on sound. It also reads off labels when hovered over with the cursor. 

You can also increase/decrease the sample area of the cursor. A bigger cursor helps you get a general idea of the image, a smaller cursor allows you to make out more details.

It uses a diagram from Anti-Oedipus as an example. This diagram can be found in section 4.1 of the book.

2. Controls

2.a. Keyboard Controls:
- Enter: Change Input
- Directional Keys: Move
- Q/W: Change Cursor Size
- A/S: Rotate Diagram
- E: Text to Speech
- R: Toggle Labels
- T: Read Controls
- Y: Toggle Audio
- U: Toggle Vibration
- I: Toggle Mono
- ESC: Exit
- Space: Next Diagram

2.b. Controller controls:
- Enter/Start: Change Input
- Left Axis: Move Cursor
- Right Axis: Change Cursor Size
- A: Text to Speech
- B: Toggle Labels
- X: Toggle Audio
- Y: Toggle Vibration
- L1: Toggle Mono
- R1: Read Controls
- L2: Next Diagram
- Select: Exit

2.c. Mouse Controls:
- Enter: Change Input
- Movement: Move Cursor
- Scroll: Change Cursor Size
- Movement + M1: Rotate Diagram
- E: Text to Speech
- R: Toggle Labels
- T: Read Controls
- Y: Toggle Audio
- U: Toggle Vibration
- I: Toggle Mono
- ESC: Exit
- Space: Next Diagram

3. Changelog

10/25/2023
- Added ability to rotate the diagram (not in Controller mode yet)

10/22/2023
- Added ability to control the two tones (for example, can make colors inverted, gold-blue, ect.). Currently only works internally in code. Will add menu later. Currently inverted color scheme.
- Added an additional diagram for testing.
- Moved text triggers where they correlate with the diagram rather than where the label is on the diagram.
- Added title screen for each diagram

10/20/2023
- Fixed bug that causes keyboard mode to fail in VM.
- Changed pitch to be inverted. Changed readme to be a text file.

10/?/2023
Some time is recorded in my repository, I don't give a shit. 
- Initial release.

4. Credits

- Prototype by PunishedFelix.
- Tested by MysticGamer23 and Haily Merry.