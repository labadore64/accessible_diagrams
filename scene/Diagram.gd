extends Control

# Export variables

export(String,MULTILINE) var intro_text = "Press Any Key/Button to Continue."

# Constants
# The strings for controls
const controls_strings = [
	"Enter - Change Input\nDirectional Keys - Move\nQ/W - Change Cursor Size\nA/S - Rotate Diagram\nE - Text to Speech\nR - Toggle Labels\nT - Read Controls\nY - Toggle Audio\nU - Toggle Vibration\nI - Mono/Stereo\nESC - Exit\nSpace - Next Diagram",
	"Enter/Start - Change Input\nLeft Axis - Move Cursor\nRight Axis - Change Cursor Size\nA/S - Rotate Diagram\nA - Text to Speech\nB - Toggle Labels\nX - Toggle Audio\nY - Toggle Vibration\nL1 - Mono/Stereo\nR1 - Read Controls\nSelect - Exit\nHome - Next Diagram",
	"Enter - Change Input\nMovement - Move Cursor\nScroll - Change Cursor Size\nClick-Rotate - Rotate Diagram\nE - Text to Speech\nR - Toggle Labels\nT - Read Controls\nY - Toggle Audio\nU - Toggle Vibration\nI - Mono/Stereo\nESC - Exit\nSpace - Next Diagram",
]

# The strings for control schemes
const input_strings = [
	"Keyboard Controls",
	"Controller Controls",
	"Mouse Controls"
]

# Stores various values for managing color changing
onready var imgMainMaterial = ($MainImg.material as ShaderMaterial)		# Stores the shader material for the image
onready var background = $Background									# Stores the background object for the image
onready var labelList = []												# Stores each of the labels
onready var stringList = []												# Stores the rest of the strings

# Called when the node enters the scene tree for the first time.
func _ready():
	$Cursor.set_texture($MainImg.texture)
	$Cursor.texture_scale = $MainImg.rect_scale
	$Cursor.texture_position = $MainImg.rect_position
	
	for c in $Labels.get_children():
		c.cursor = $Cursor
		labelList.append(c)
		
	$Cursor.label_list = labelList
		
	stringList.append($Controls)
	stringList.append($ControlsText)
	stringList.append($Preview/ControlsText)
	stringList.append($Preview/Title)
	stringList.append($DemoName)
	stringList.append($Preview/DemoName)
	
	$Preview/DemoName.text = $DemoName.text
		
	loadControls(false)
	loadDefaultColors()
	
	readLabel()
	
# Reads the introduction text
func readLabel():
	TTS.say($Preview/Title.text + " Demo name: " + $Preview/DemoName.text + ". " + $Preview/ControlsText.text)
	
func loadControls(tts_say=true):
	$ControlsText.text = controls_strings[$Cursor.input_mode];
	$Controls.text = input_strings[$Cursor.input_mode]
	$Preview/Title.text = intro_text
	$Preview/ControlsText.text = $Controls.text + "\n\n" + controls_strings[$Cursor.INPUT_STATE.KEYBOARD]
	
	if tts_say:
		$Cursor.tts_say($Controls.text)
	
func loadDefaultColors():
	set_foreground_color(Color.white)
	set_background_color(Color.black)

func _input(event):
	# If preview is visible, do preview code, otherwise do cursor code
	if $Preview.visible:
		$Preview.do_input(event)
	else:
		$Cursor.do_input(event)

func _process(delta):
	
	# Quits if exit is pressed
	if Input.is_action_just_pressed("exit"):
		get_tree().quit()
		
	# If input is pressed, change input type
	elif Input.is_action_just_pressed("input"):
		$Cursor.update_input()
		loadControls()
		
	elif Input.is_action_just_pressed("read_controls"):
		$Cursor.tts_say($ControlsText.text)
		
	# Other routines
	else:
		# If preview is visible, do preview code, otherwise do cursor code
		if $Preview.visible:
			$Preview.do_process(delta)
		else:
			$Cursor.do_process(delta)

## Theme color functions

# Sets the background color
func set_background_color(color):
	background.color = color
	$Preview/Background.color = color
	get_parent().get_node("BG").color = color

# Sets the foreground color
func set_foreground_color(color):
	if imgMainMaterial != null:
		imgMainMaterial.set_shader_param("color", color)
	for c in labelList:
		c.add_color_override("font_color", color)
	for c in stringList:
		c.add_color_override("font_color", color)
		
	$Cursor.cursor_display_color = color
