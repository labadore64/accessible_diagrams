extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Does the process event.
func do_process(delta):
	pass

# Does the event process
# Just removes itself when clicked or button is pushed
func do_input(event):
	if event is InputEventJoypadButton || event is InputEventMouseButton || event is InputEventKey:
		visible = false
		Input.action_release("change_diagram")
		TTS.stop()
		TTS.speak("Showing diagram")
