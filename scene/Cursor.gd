extends Control

# If input is received this frame, set to true.
var input_received_movement = false;				# If true, input was received for movement this frame
var input_received_radius = false;					# If true, input was received for radius size change this frame
var input_received_tts = false						# If true, input was received for enable/disable TTS this frame
var input_received_toggle_audio	= false				# If true, input was received for toggle audio this frame
var input_received_toggle_vibration	= false			# If true, input was received for toggle vibration this frame
var input_received_toggle_labels = false			# If true, input was received for toggle labels this frame
var input_received_mono_mode = false				# If true, input was received for toggle mono mode this frame
var input_received_tab = false						# If true, input was received for tabbing this frame
var input_received_rotation = false;				# If true, input was received for rotation this frame

# These values fully control input of the cursor
var radius = 10										# Radius of the cursor
var position = Vector2.ZERO							# Position of the cursor
var speed = 4										# Speed of the cursor

# These values fully control output of the cursor
var cursor_measured = [];							# Contains all the cursor points measured with the sample
var cursor_position = Vector2.ZERO 					# Represents the cursor position 0.0 - 1.0
var cursor_display_color = CURSOR_BOUNDARY_COLOR	# Color of the cursor
var cursor_color = Color(0,0,0,1)					# Gets the average color of the cursor points
var contrast_factor = 1								# The amount of contrast to add given the size of the sample
var cursor_values = {								# The rgba/hsv values of the cursor
	"red" : 0,
	"green" : 0,
	"blue" : 0,
	"hue" : 0,
	"sat" : 0,
	"value" : 0,
	"alpha" : 0
}

# Mouse variables used to control input
var mouse_clicked_position = Vector2.ZERO			# Mouse position when clicked.
var mouse_original_rotation = 0;					# Original rotation when clicked.

# This is the texture the cursor is looking at
var texture = null									# The actual texture
var texture_scale = Vector2.ONE						# The scale of the texture in a Vector2
var texture_position = Vector2.ZERO					# The position of the texture in a Vector2
var texture_width = 1;								# The width of the texture in pixels
var texture_height = 1;								# The height of the texture in pixels
var img = null										# The image generated for the texture (used for pixel hunting)

# This is for controlling input
var input_mode = INPUT_STATE.KEYBOARD				# The current input state

# This is for controlling audio output
var audio_enabled = true							# If true, audio is output
var vibration_enabled = true						# If true, vibration in output
var can_speak = false								# If true, TTS enabled
var mono_mode = false								# If true, sound is mono
var labels_speak = true								# If true, reads the labels when hovered over
var radius_changed = false							# If true, radius is being changed this frame
var radius_changed_last = false						# If true, radius was changed last frame
var radius_sound_paused = 0							# If 0, plays the sound. acts as a stack to avoid cutting off the sound.

# Stores the parent diagram
onready var parent_diagram = get_parent()

# This is for controlling previous/next input for labels
var label_current = 0								# Index of the current label
onready var label_list = parent_diagram.labelList	# Stores the reference to the label list

# Constants
const MIN_RADIUS = 2.0								# Minimum cursor radius	(should be float)
const MAX_RADIUS = 50.0								# Maximum cursor radius (should be float)
const RADIUS_INTERVAL = 2							# Interval of incrementing/decrementing radius size
const CURSOR_BOUNDARY_COLOR = Color(1.0,0.0,0.0)	# Boundary color of cursor (used for debugging)
const CONTRAST_MODIFIER = 5							# Contrast modifier
const PITCH_SHIFTER_X = 0.15						# How much the pitch shifts with x value
const PITCH_SHIFTER_Y = 0.35						# How much the pitch shifts with y value
const PITCH_SHIFTER_MONO = 0.25						# How much the pitch shifts with x-y value in mono-mode
const PITCH_SHIFTER_RADIUS = 0.45					# How much the pitch shifts with radius changes
const PITCH_SHIFTER_ROTATION = 0.5					# How much the pitch shifts with rotation changes
const VIBRATION_SHIFTER = 0.5						# The multiplier for vibration
const KEYBOARD_ROTATION_SPEED = 5					# Base keyboard rotation speed
const MOUSE_ROTATION_SPEED = 2						# Base mouse rotation speed
const CONTROLLER_ROTATION_SPEED = 5					# Base controller rotation speed

# String consts
const TTS_ENABLED = "TTS Enabled"
const TTS_DISABLED = "TTS Disabled"
const AUDIO_ENABLED = "Audio Enabled"
const AUDIO_DISABLED = "Audio Disabled"
const LABELS_ENABLED = "Labels Enabled"
const LABELS_DISABLED = "Labels Disabled"
const VIBR_ENABLED = "Vibration Enabled"
const VIBR_DISABLED = "Vibration Disabled"
const MONO_ENABLED = "Mono Audio"
const MONO_DISABLED = "Stereo Audio"

# Enum
enum INPUT_STATE {
	KEYBOARD,
	CONTROLLER,
	MOUSE
}

# Mouse variables
var mouse = Vector2.ZERO							# The current mouse position
var last_mouse = Vector2.ZERO						# The last mouse position

# Processes when ready
func _ready():
	input_received_movement = true
	position = get_viewport_rect().size / 2
	
	# wait a bit to read off TTS
	yield(get_tree().create_timer(0.1),"timeout")
	
	can_speak = true

# Processes input
func do_input(event):
	
	if Input.is_action_just_pressed("ui_focus_next") || Input.is_action_just_pressed("ui_focus_prev"):
		if !input_received_tab:
			input_received_tab = input_tab(event)
	else:
		if input_mode == INPUT_STATE.MOUSE:
			input_mouse(event)
		elif input_mode == INPUT_STATE.CONTROLLER:
			input_controller(event)
		elif input_mode == INPUT_STATE.KEYBOARD:
			input_keyboard(event)
			
# Processes each frame
func do_process(delta):
	# Sends an update to the cursor
	if input_received_movement || input_received_radius || input_received_tab || input_received_rotation:
		cursor_update()
		
	# Updates the sound of the radius
	if radius_changed:
		if audio_enabled:
			if !$radius.playing:
				$radius.play()
			$radius.pitch_scale = 0.75 - PITCH_SHIFTER_RADIUS + PITCH_SHIFTER_RADIUS * ( 1 - (radius - MIN_RADIUS)/(MAX_RADIUS - MIN_RADIUS) )

	if !radius_sound_paused:
		if !radius_changed:
			
			radius_sound_paused += 1
			# Reset if input is received.
			input_received_movement = false
			input_received_radius = false
			input_received_tts = false
			input_received_toggle_audio	= false
			input_received_toggle_vibration	= false
			input_received_toggle_labels = false
			input_received_mono_mode = false
			input_received_tab = false
			input_received_rotation = false;

	
			# Reset radius movement
			radius_changed_last = radius_changed;
			radius_changed = false;
			
			yield(get_tree().create_timer(0.5),"timeout")
			radius_sound_paused -=1
			if radius_sound_paused == 0:
				$radius.stop()
		
	# Reset if input is received.
	input_received_movement = false
	input_received_radius = false
	input_received_tts = false
	input_received_toggle_audio	= false
	input_received_toggle_vibration	= false
	input_received_toggle_labels = false
	input_received_mono_mode = false
	input_received_tab = false
	input_received_rotation = false;	
	
	# Reset radius movement
	radius_changed_last = radius_changed;
	radius_changed = false;
	

# Draws when updated
func _draw():
	# Draws the circle representing the selected cursor color
	draw_circle(position,radius,cursor_color)
	
	# Draws the circle radius
	draw_arc(position, radius, 0, 2*PI, 50, cursor_display_color, 2)

## Update methods

# Updates the cursor
func cursor_update():
	# Updates the draw cycle
	update()
	cursor_update_state()
	
	$Detection.position = position

# Updates the internal state of the cursor
func cursor_update_state():
	# Sets the contrast factor
	contrast_factor = 1 + CONTRAST_MODIFIER * ( (radius * 1.0) / MAX_RADIUS )

	# Sets the color
	calculate_color()
	
	# Calculates the color values
	calculate_color_values()
	
	# Calculates output
	do_output()

## Input Methods

# Input for tabbing
func input_tab(event):
	
	var moved = false
	
	if Input.is_action_just_pressed("ui_focus_prev"):
		moved = true
		label_current -= 1
	elif Input.is_action_just_pressed("ui_focus_next"):
		moved = true
		label_current += 1
		
	if label_current < 0:
		label_current = label_list.size()-1
	elif label_current >= label_list.size():
		label_current = 0
			
	position = label_list[label_current].get_midpoint()
		
	TTS.clear()
	TTS.say(label_list[label_current].text)
		
	return moved

# Input for mouse
func input_mouse(event):
	
	# Sets mouse rotation
	if Input.is_action_just_pressed("rotation_hold_mouse"):
		mouse_clicked_position = get_viewport().get_mouse_position()
		mouse_original_rotation = get_parent().rect_rotation;
	
	# If movement input has not been received this frame yet
	if !input_received_movement:
		# Sets the cursor position based on mouse position
		if event is InputEventMouseMotion:
			input_received_movement = input_mouse_motion(event)
			
	# If radius input has not been received for this frame yet
	if !input_received_radius:
		# Sets the radius with the mouse
		if event is InputEventMouseButton:
			input_received_radius = input_mouse_button(event)
			
	# TTS Enabled
	# If tts input has not been received for this frame yet
	if !input_received_tts:
		input_received_tts = input_mouse_tts(event)
		
	# Audio Enabled
	if !input_received_toggle_audio:
		input_received_toggle_audio = input_keyboard_audio()

	# Labels Enabled
	if !input_received_toggle_labels:
		input_received_toggle_labels = input_keyboard_labels()

	# Vibration Enabled
	if !input_received_toggle_vibration:
		input_received_toggle_vibration = input_controller_vibration()
	
	# Mono Mode
	if !input_received_mono_mode:
		input_received_mono_mode = input_keyboard_monomode()
	
	# Rotation
	if !input_received_rotation:
		input_received_rotation = input_mouse_rotation(event);
		
# Detects input from the mouse motion.
# Sets cursor position.
# Returns if interaction was processed
func input_mouse_motion(event):
	last_mouse = mouse
	mouse = event.position
	position = get_parent().get_node("PIVOT").to_local(mouse)
	return mouse != last_mouse
	
# Sets rotation based on mouse position
func input_mouse_rotation(event):
	if event is InputEventMouseMotion:
		if Input.is_action_pressed("rotation_hold_mouse"):
			var rotation = rad2deg((get_viewport().get_mouse_position() - get_parent().rect_pivot_offset).angle())
			print(rotation)
			get_parent().rect_rotation = rotation
	
# Detects input from mouse buttons.
# Sets cursor radius.
# Returns if mouse button was processed
func input_mouse_button(event):
	if event.button_index == BUTTON_WHEEL_UP:
		radius += RADIUS_INTERVAL
		radius = clamp(radius,MIN_RADIUS,MAX_RADIUS)
		radius_changed = true
		return true
	elif event.button_index == BUTTON_WHEEL_DOWN:
		radius -= RADIUS_INTERVAL
		radius = clamp(radius,MIN_RADIUS,MAX_RADIUS)
		radius_changed = true
		return true
		
	# Returns false if no interaction
	return false

# Detects input for enabling/disabling TTS
# Returns if mouse button was processed
func input_mouse_tts(event):
	if Input.is_action_just_pressed("tts_enable"):
		can_speak = !can_speak
		if can_speak:
			TTS.say(TTS_ENABLED)
		else:
			TTS.say(TTS_DISABLED)
		return true
		
	return false

# Input for controller
func input_controller(event):
	
	# If input is keyboard
	if event is InputEventJoypadButton || event is InputEventJoypadMotion:
	
		# Directional movement
		# Only move if no movement previously detected
		if !input_received_movement:
			var direction = Vector2.ZERO
			
			# Apply movement
			direction.x = Input.get_action_strength("cursor_right") - Input.get_action_strength("cursor_left")
			direction.y = Input.get_action_strength("cursor_down") - Input.get_action_strength("cursor_up")
			
			# If input is digital, normalize it for diagonal movement
			if abs(direction.x) == 1 and abs(direction.y) == 1:
				direction = direction.normalized()
				
			if direction != Vector2.ZERO:
				input_received_movement = true
				position += direction * speed
				position = Vector2(
							clamp(position.x,0,rect_size.x),
							clamp(position.y,0,rect_size.y)
				)
			
		# Radius size
		# Only move if no previous radius size changes detected
		if !input_received_radius:
			input_received_radius = input_keyboard_radius()

		# TTS Enabled
		# If tts input has not been received for this frame yet
		if !input_received_tts:
			input_received_tts = input_mouse_tts(event)
			
		# Audio Enabled
		if !input_received_toggle_audio:
			input_received_toggle_audio = input_keyboard_audio()

		# Labels Enabled
		if !input_received_toggle_labels:
			input_received_toggle_labels = input_keyboard_labels()

		# Vibration Enabled
		if !input_received_toggle_vibration:
			input_received_toggle_vibration = input_controller_vibration()
	
		# Mono Mode
		if !input_received_mono_mode:
			input_received_mono_mode = input_keyboard_monomode()
	
# Gets the input for toggling vibration input
func input_controller_vibration():
	if Input.is_action_pressed("toggle_vibration"):
		vibration_enabled = !vibration_enabled
		if vibration_enabled:
			TTS.say(VIBR_ENABLED)
		else:
			TTS.say(VIBR_DISABLED)
		return true
		
	return false

# Input for keyboard
func input_keyboard(event):
	
	# If input is keyboard
	if event is InputEventKey:
	
		# Directional movement
		# Only move if no movement previously detected
		if !input_received_movement:
			var direction = Vector2.ZERO
			
			# Apply movement
			direction.x = Input.get_action_strength("cursor_right") - Input.get_action_strength("cursor_left")
			direction.y = Input.get_action_strength("cursor_down") - Input.get_action_strength("cursor_up")
			
			# If input is digital, normalize it for diagonal movement
			if abs(direction.x) == 1 and abs(direction.y) == 1:
				direction = direction.normalized()
				
			direction = direction.rotated(-deg2rad(get_parent().rect_rotation))
				
			if direction != Vector2.ZERO:
				input_received_movement = true
				position += direction * speed
				position = Vector2(
							clamp(position.x,rect_position.x,rect_size.x),
							clamp(position.y,rect_position.y,rect_size.y)
				)
				
				# Play bonk sound
				if position.x == rect_position.x || position.x == rect_size.x || position.y == rect_position.y || position.y == rect_size.y:
					if !$bonk.playing:
						$bonk.play()
			
		# Radius size
		# Only move if no previous radius size changes detected
		if !input_received_radius:
			input_received_radius = input_keyboard_radius()

		# TTS Enabled
		# If tts input has not been received for this frame yet
		if !input_received_tts:
			input_received_tts = input_mouse_tts(event)
			
		# Audio Enabled
		if !input_received_toggle_audio:
			input_received_toggle_audio = input_keyboard_audio()

		# Labels Enabled
		if !input_received_toggle_labels:
			input_received_toggle_labels = input_keyboard_labels()

		# Vibration Enabled
		if !input_received_toggle_vibration:
			input_received_toggle_vibration = input_controller_vibration()
			
		# Mono Mode
		if !input_received_mono_mode:
			input_received_mono_mode = input_keyboard_monomode()
		
		# Rotation
		if !input_received_rotation:
			input_received_rotation = input_keyboard_rotation()

	

# Gets the input for increasing/decreasing radius size for the keyboard/controller		
func input_keyboard_radius():
	if Input.is_action_pressed("cursor_big"):
		radius += RADIUS_INTERVAL
		radius = clamp(radius,MIN_RADIUS,MAX_RADIUS)
		radius_changed = true
		return true
	elif Input.is_action_pressed("cursor_small"):
		radius -= RADIUS_INTERVAL
		radius = clamp(radius,MIN_RADIUS,MAX_RADIUS)
		radius_changed = true
		return true
	return false
	
# Gets the input for toggling audio input
func input_keyboard_audio():
	if Input.is_action_pressed("toggle_audio"):
		audio_enabled = !audio_enabled
		if audio_enabled:
			TTS.say(AUDIO_ENABLED)
		else:
			TTS.say(AUDIO_DISABLED)
		return true
		
	return false
	
# Gets the input for toggling labels
func input_keyboard_labels():
	if Input.is_action_pressed("read_labels"):
		labels_speak = !labels_speak
		if labels_speak:
			TTS.say(LABELS_ENABLED)
		else:
			TTS.say(LABELS_DISABLED)
		return true
		
	return false

# Gets the input for toggling labels
func input_keyboard_monomode():
	if Input.is_action_pressed("mono_mode"):
		mono_mode = !mono_mode
		if mono_mode:
			TTS.say(MONO_ENABLED)
		else:
			TTS.say(MONO_DISABLED)
		return true
		
	return false

# Gets the keyboard input for rotation
func input_keyboard_rotation():
	if Input.is_action_pressed("rotate_clockwise"):
		get_parent().rect_rotation += KEYBOARD_ROTATION_SPEED
		get_parent().rect_rotation = fmod((get_parent().rect_rotation + 360),360.0)
		return true
	elif Input.is_action_pressed("rotate_counterclockwise"):
		get_parent().rect_rotation -= KEYBOARD_ROTATION_SPEED
		get_parent().rect_rotation = fmod((get_parent().rect_rotation + 360),360.0)
		return true
		
	return false

## Texture functions

func set_texture(text):
	texture = text
	texture_width = text.get_width() -1;
	texture_height = text.get_height() - 1;
	
	# Retrieve the captured image.
	img = texture.get_data()

# Calculates the color of the current cursor selection
func calculate_color():
	if img != null:
		
		# Clears the cursor_measured list
		cursor_measured.clear()
		cursor_color = Color(0,0,0,0)
		
		cursor_position = Vector2(
								((position.x - texture_position.x) / texture_scale.x) / texture_width,
								((position.y - texture_position.y) / texture_scale.y) / texture_height
								)
		
		
		# Locks image, then gets all the values
		img.lock()
		
		var rad5 = radius
		
		for i in range(-rad5,rad5):
			for j in range(-rad5,rad5): 
				
				var textx = ( position.x - texture_position.x + i ) / texture_scale.x;
				var texty = ( position.y - texture_position.y + j ) / texture_scale.y
				
				if textx > 0 && textx < texture_width:
					if texty > 0 && texty < texture_height:
						cursor_measured.append(img.get_pixel(textx,texty))
		img.unlock()
		
		# Gets the average color
		for c in cursor_measured:
			cursor_color += c
			
		# Makes the cursor color the same as the grayscale of the shader
		var avg = (cursor_color.r + cursor_color.g + cursor_color.b) / 765
		cursor_color = Color(0.0, 0.0, 0.0, (1.0 - avg) * cursor_color.a)
			
		if cursor_measured.empty():
			cursor_color = Color(0,0,0,0)
		else:
			cursor_color = (cursor_color / cursor_measured.size()) * contrast_factor

# Calculates the color values after getting cursor color
func calculate_color_values():
	cursor_values["red"] = clamp(cursor_color.r,0.0,1.0)
	cursor_values["green"] = clamp(cursor_color.g,0.0,1.0)
	cursor_values["blue"] = clamp(cursor_color.b,0.0,1.0)
	cursor_values["hue"] = clamp(cursor_color.h,0.0,1.0)
	cursor_values["sat"] = clamp(cursor_color.s,0.0,1.0)
	cursor_values["value"] = clamp(cursor_color.v,0.0,1.0)
	cursor_values["alpha"] = clamp(cursor_color.a,0.0,1.0)

## Input methods

# Updates the input
func update_input():
	input_mode+=1
		
	# Skip controller if controller is not detected
	if input_mode == INPUT_STATE.CONTROLLER:
		if Input.get_connected_joypads().empty():
			input_mode+=1;
			
	if input_mode >= INPUT_STATE.size():
		input_mode = 0

## Output methods

# General output update method
func do_output():
	audio_output()
	vibration_output()

## Audio output methods

# Calculates audio output.
# Currently demo only outputs alpha.
func audio_output():
	
	# If in mono mode
	if mono_mode:
		$value.pitch_scale = 1 - PITCH_SHIFTER_MONO*2 + (PITCH_SHIFTER_MONO * 2 * (cursor_position.x)) + (PITCH_SHIFTER_MONO * 2 * (1-cursor_position.y))
	else:
		# Updates the pitch of the value to be the y cursor position
		$value.pitch_scale = 1 - PITCH_SHIFTER_Y + (PITCH_SHIFTER_Y * 2 * (1-cursor_position.y))
		
	# Updates the panning to match the value of the x cursor position
	audio_pan((2 * cursor_position.x) - 1)
	
	$value.volume_db = -80
	
	if audio_enabled:
		audio_rotation_output()
		
		$value.volume_db = (1.0 - sqrt(cursor_values["alpha"])* (1 - cursor_values["value"])) * -20
		if $value.volume_db <= -20.0:
			$value.volume_db = -80

# Calculates rotation audio output
func audio_rotation_output():
	if Input.is_action_just_released("rotate_clockwise") || Input.is_action_just_released("rotate_counterclockwise") || Input.is_action_just_released("rotation_hold_mouse"):
		if $rotation.playing:
			$rotation.stop()
			
	if Input.is_action_just_pressed("rotate_clockwise") || Input.is_action_just_pressed("rotate_counterclockwise") || Input.is_action_just_pressed("rotation_hold_mouse"):
		if !$rotation.playing:
			$rotation.play()
	$rotation.pitch_scale = ((fmod(360.0 + get_parent().rect_rotation, 360.0) ) / 360.0) + 0.5
	
# Sets the master volume panning.
func audio_pan(value):
	var effect = AudioServer.get_bus_effect(0, 0)
	if mono_mode:
		effect.pan = 0
	else:
		effect.pan = clamp(value,-1,1)
	

## Vibration output methods

# Calculates vibration output
# Should decay after a short period as to not overwhelm the user.
# Currently demo only outputs alpha.
func vibration_output():
	
	if vibration_enabled:
		
		# multiplier
		var multi = sqrt(cursor_values["alpha"] * (1 - cursor_values["value"])) * VIBRATION_SHIFTER
		
		# Gets the scaling for the two dimensions
		# X correlates with direction
		# Y correlates with 
		var scaling = Vector2(
							clamp((cursor_position.x),0,1),
							clamp((cursor_position.y),0,1)
							);
						
		# normalize scaling
		scaling = scaling.normalized() * multi
							
		Input.start_joy_vibration(0,scaling.x,scaling.y,0.25)

## TTS functions

# Says TTS taking into account if TTS is enabled
func tts_say(words):
	if can_speak:
		words = words.replace("\n"," ")
		TTS.say(words)
