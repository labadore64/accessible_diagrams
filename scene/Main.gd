extends Control

const diagrams = [
	preload("res://scene/Diagram_AO1.tscn"),
	preload("res://scene/Diagram_AO2.tscn")
]

var diagram_index = -1;

func _ready():
	load_diagram()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("change_diagram"):
		yield(get_tree().create_timer(0.25), "timeout")
		load_diagram()
		
# Loads a new diagram
func load_diagram():
	# Increment the new diagram
	diagram_index += 1;
	
	if diagram_index >= diagrams.size():
		diagram_index = 0

	# Free the current diagram
	for c in get_children():
		# Don't destroy the sound effect lol
		if !(c is AudioStreamPlayer) && !(c is ColorRect):
			c.queue_free()
	
	# load the new diagram
	var new_inst = diagrams[diagram_index].instance()
	new_inst.name = "Diagram"
	
	$DiagramChange.play()
	TTS.stop()
	
	call_deferred("add_child",new_inst)
