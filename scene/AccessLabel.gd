extends Label

var cursor = null

func _on_Area2D_area_entered(area):
	if cursor.labels_speak:
		cursor.tts_say(text)

func _on_Area2D_area_exited(area):
	pass # Replace with function body.

func get_midpoint():
	# Handle rectangle 2D shape special
	if get_child(0).get_child(0) is CollisionShape2D:
		return get_child(0).get_child(0).shape.extents * 0.5 + get_child(0).get_child(0).global_position 
	else:
		return Vector2(0,0)
