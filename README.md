
# Introduction

This project is a small accessibility demo to demonstrate a diagram reader for blind and visually impaired people.

Move the cursor around - as you go over the diagram image, you will hear a sound that correlates with your position. This sound is both panned for X axis, and pitched for Y axis, so you can tell your position based on sound. It also reads off labels when hovered over with the cursor. 

You can also increase/decrease the sample area of the cursor. A bigger cursor helps you get a general idea of the image, a smaller cursor allows you to make out more details.

It uses a diagram from Anti-Oedipus as an example. This diagram can be found in section 4.1 of the book. Actually, now it displays both diagrams from here, and you can swap between them. Spicy.

![Diagram that the demo uses](diagram_img.png)

# Controls

## Keyboard Controls:
- **Enter**: Change Input
- **Directional Keys**: Move
- **Q/W**: Change Cursor Size
- **E**: Text to Speech
- **R**: Toggle Labels
- **T**: Read Controls
- **Y**: Toggle Audio
- **U**: Toggle Vibration
- **I**: Toggle Mono
- **ESC**: Exit
- **Space:** Next Diagram

## Controller controls:
- **Enter/Start**: Change Input
- **Left Axis**: Move Cursor
- **Right Axis**: Change Cursor Size
- **A**: Text to Speech
- **B**: Toggle Labels
- **X**: Toggle Audio
- **Y**: Toggle Vibration
- **L1**: Toggle Mono
- **R1**: Read Controls
- **L2:** Next Diagram
- **Select**: Exit

## Mouse Controls:
- **Enter**: Change Input
- **Movement**: Move Cursor
- **Scroll**: Change Cursor Size
- **E**: Text to Speech
- **R**: Toggle Labels
- **T**: Read Controls
- **Y**: Toggle Audio
- **U**: Toggle Vibration
- **I**: Toggle Mono
- **ESC**: Exit
- **Space:** Next Diagram

# Credits

- Prototype by PunishedFelix.
- Tested by MysticGamer23 and Haily Merry.